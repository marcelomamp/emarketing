<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            [
                'name' => "ADMIN",
                'description' => 'User responsible for create all other users and manage them'
            ],
            [
                'name' => "JOURNALIST",
                'description' => "User responsible for create the newsletters and send them"
            ]  
        ]); 
    }
}
