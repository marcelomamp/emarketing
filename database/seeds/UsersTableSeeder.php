<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => "Administrator",
            'email' => 'admin@emarketing.fr',
            'email_verified_at' => "2018-09-17 00:00:00",
            'password' => bcrypt('mju7MJU&'),
            'role_id' => 1
        ]);
    }
}
